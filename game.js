var state = 'car';
var generator = false;
var light = false;

var syslog = '<b>Системный лог</b><br>—';

var pre_text = '';
var pre_log = '';
var pre_variants = '';

var glovebox_sound = new Audio('res/snd/glovebox.wav');
var exit_car_sound = new Audio('res/snd/exit_car.wav');
var click_sound = new Audio('res/snd/click.mp3');
var facade2back_sound = new Audio('res/snd/facade2back.wav');
var door_sound = new Audio('res/snd/door.mp3');

function scan() {
    $.getJSON("res/data.json",
        function (data) {
            if (read().includes('debug')) {
                $('#debug_button').show();
                syslog_write('<br><i>Debug Меню активировано</i><br>—');
            }
            switch (state) {
                case 'car': {
                    write(data.car.write);
                    log(data.car.log);
                    variants(data.car.variants);
                    if (read().includes(data.car.input_look)) {
                        log(data.car.log_look);
                    }
                    if (read().includes(data.car.input_watch)) {
                        log(data.car.log_watch);
                    }
                    if (read().includes(data.car.input_glove)) {
                        glovebox_sound.play();
                        log(data.car.log_glove);
                        changeState('glove');
                    }
                    break;
                }

                case 'glove': {
                    write(data.glove.write);
                    variants(data.glove.variants);
                    if (read().includes(data.glove.input_letter)) {
                        log(data.glove.log_letter);
                    }
                    if (read().includes(data.glove.input_facade)) {
                        exit_car_sound.play();
                        log(data.glove.log_facade);
                        changeState('facade');
                    }
                    break;
                }

                case 'facade': {
                    write(data.facade.write);
                    variants(data.facade.variants);
                    if (read().includes(data.facade.input_back)) {
                        facade2back_sound.play();
                        log(data.facade.log_back);
                        changeState('back');
                    }
                    if (read().includes(data.facade.input_house)) {
                        door_sound.play();
                        log(data.facade.log_house);
                        changeState('house');
                    }
                    break;
                }

                case 'back': {
                    write(data.back.write);
                    variants(data.back.variants);
                    if (!generator) {
                        if (read().includes(data.back.input_generator)) {
                            log(data.back.log_generator);
                            turnGenerator(true);
                        }
                    } else {
                        if (read().includes(data.back.input_generator)) {
                            log(data.back.log_generator2);
                        }
                    }
                    if (read().includes(data.back.input_facade)) {
                        facade2back_sound.play();
                        log(data.back.log_facade);
                        changeState('facade');
                    } else {
                    }
                    break;
                }

                case 'house': {
                    if (read().includes(data.house.input_facade)) {
                        door_sound.play();
                        log(data.house.log_facade);
                        changeState('facade');
                    }
                    if (light) {
                        write(data.house.write_demo);
                        variants(data.house.variants_demo);
                        if (read().includes(data.house.input_light_off)) {
                            log(data.house.log_light_off);
                            turnLight(false);
                        }
                    } else {
                        write(data.house.write_dark);
                        variants(data.house.variants_dark);
                        if (read().includes(data.house.input_light)) {
                            if (generator) {
                                log(data.house.log_light);
                                turnLight(true);
                            } else if (!generator) {
                                log(data.house.log_light_no_gen);
                            }
                        }
                    }
                    break;
                }
            }
            write_cookie();
            $('#input').val('');
        });
}

function changeState(text) {
    state = text;
    debug_redraw();
    scan();
    syslog_write('<br>Сцена: ' + text + '<br>—');
}

function turnGenerator(text) {
    generator = text;
    click_sound.play();
    debug_redraw();
    scan();
    syslog_write('<br>Генератор: ' + generator + '<br>—');
}

function turnLight(text) {
    light = text;
    click_sound.play();
    debug_redraw();
    scan();
    syslog_write('<br>Свет: ' + light + '<br>—');
}

function restart() {
    syslog = '<b>Системный лог</b><br>—';
    syslog_write('<br><i>Начало новой игры</i><br>—');
    changeState('car');
    turnGenerator(false);
    turnLight(false);
    scan();
}

function initialize() {
    $.getJSON("res/data.json",
        function (data) {
            load_cookie(data.game.cookie_info);
            debug_redraw();

            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                $('#mobile_info').html(data.game.mobile_info);
                $('#mobile_alert').show();
                syslog_write('<br><i>Пользователь зашёл с мобильного устройства</i><br>—');
                $('#report_button').remove();
            } else {
                syslog_write('<br><i>Пользователь зашёл с ПК</i><br>—');
            }

            document.title = data.game.title;
            $('#game_name').html(data.game.title);
            $('#game_version').html(data.game.version);
            $('#game_developer').html(data.game.developer);
            $('#changelog').html(data.game.changelog);

            $('#loading_screen').hide();
            $('#all_page').show();

            $('#input').on('keyup', function (e) {
                if (e.keyCode == 13) {
                    syslog_write('<br>Ввод: ' + read() + '<br>—');
                    scan();
                }
            });
        });
}
