function write(text) {
    if (text != pre_text) {
        pre_text = text;
        $('#output').empty();
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            new TypeIt('#output', {
                strings: text,
                startDelay: 0,
                speed: 1,
                nextStringDelay: 50,
                callback: function () {
                    $("html, body").animate({ scrollTop: $(document).height() }, 250);
                }
            });
        } else {
            new TypeIt('#output', {
                strings: text,
                startDelay: 0,
                speed: 10,
                callback: function () {
                    $("html, body").animate({ scrollTop: $(document).height() }, 500);
                }
            });
        }
    }
}

function log(text) {
    if (text != pre_log) {
        pre_log = text;
        $('#log').empty();
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            new TypeIt('#log', {
                strings: text,
                startDelay: 0,
                speed: 1,
                cursor: false,
                nextStringDelay: 50,
                callback: function () {
                    $("html, body").animate({
                        scrollTop: $("#log_top").position().top
                    }, 500);
                }
            });
        } else {
            new TypeIt('#log', {
                strings: text,
                startDelay: 0,
                speed: 10,
                cursor: false,
                callback: function () {
                    $("html, body").animate({
                        scrollTop: $("#log_top").position().top
                    }, 500);
                }
            });
        }
    }
}

function variants(text) {
    if (text != pre_variants) {
        pre_variants = text;
        f_text = '&nbsp;&nbsp;' + text;
        $('#variants').empty();
        new TypeIt('#variants', {
            strings: f_text,
            startDelay: 0,
            speed: 10,
            cursor: false
        });
    }
}
