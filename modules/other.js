function string2Bool(string) {
    var bool;
    bool = (function () {
        switch (false) {
            case string.toLowerCase() !== 'true':
                return true;
            case string.toLowerCase() !== 'false':
                return false;
        }
    })();
    if (typeof bool === "boolean") {
        return bool;
    }
    return void 0;
};

function read() {
    return $('#input').val().toLowerCase();
}

function report() {
    $('#report').qrcode(state + "\ng " + generator + "\nl " + light);
    $('#report_text').html("Пожалуйста, скачайте изображение и <a href='https://bitbucket.org/exDevelopment/welcomehome/issues/new' target='blank'>перейдите на сайт</a>");
    if (!$('#report_div').prop("aria-expanded")) {
        $('#report_div').collapse();
    }
}
