function debug() {
    if (state != $('#debug_state').val()) {
        state = $('#debug_state').val();
        syslog_write('<br>Сцена (<i>debug</i>): ' + state + '<br>—');
    }
    if (document.getElementById('debug_generator').checked) {
        generator = true;
        syslog_write('<br>Генератор (<i>debug</i>): true<br>—');
    } else {
        generator = false;
        syslog_write('<br>Генератор (<i>debug</i>): false<br>—');
    }
    if (document.getElementById('debug_light').checked) {
        light = true;
        syslog_write('<br>Свет (<i>debug</i>): true<br>—');
    } else {
        light = false;
        syslog_write('<br>Свет (<i>debug</i>): false<br>—');
    }
    scan();
}

function debug_redraw() {
    $('#debug_state').val(state);
    $('#debug_generator').prop('checked', generator);
    $('#debug_light').prop('checked', light);
    debug_gen();
}

function debug_gen() {
    if ($('#debug_generator').is(':checked')) {
        $('#debug_light').removeAttr("disabled");
    } else {
        $("#debug_light").attr('disabled', true);
        $('#debug_light').prop('checked', false);
    }
}

function syslog_write(text) {
    syslog += text;
    $('#syslog').html(syslog);
}

function syslog_copy() {
    var clipboard = new Clipboard('#syslog_copy_button');
}
