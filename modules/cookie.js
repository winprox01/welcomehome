function write_cookie() {
    Cookies.set('state', state, { expires: 365 });
    Cookies.set('generator', generator, { expires: 365 });
    Cookies.set('light', light, { expires: 365 });
    Cookies.set('syslog', syslog, { expires: 365 });
}

function load_cookie(text) {
    if (typeof Cookies.get('state') === 'undefined') {
        restart();
        $('#cookie_alert').show();
        $('#cookie_info').html(text);
        syslog_write('<br><i>Куки не найдены, записаны стандартные значения</i><br>—');
    } else {
        $('#cookie_alert').hide();
        syslog = Cookies.get('syslog');
        changeState(Cookies.get('state'));
        turnGenerator(string2Bool(Cookies.get('generator')));
        turnLight(string2Bool(Cookies.get('light')));
        syslog_write('<br><i>Куки загруженны</i><br>—');
        scan();
    }
}
